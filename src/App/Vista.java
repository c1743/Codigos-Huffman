/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author AndresFWilT / Edwin Hernandez
 */
public class Vista extends javax.swing.JFrame {

    /**
     * Creates new form Vista
     */
    //ArbolNario arbol = new ArbolNario();
    //Caracter raiz;
    Huffman huffman = new Huffman();
    HashMap<String, String> diccionario = new HashMap<String, String>();
    boolean draw = false;

    JTextField palabra;
    //JTextField traduccion;
    JButton insertar;
    JButton dibujar;
    JLabel a, b;
    JLabel trad;
    JTextArea mostrarInfo;
    JTextArea mostrarCodigo;
    JTextArea mostrarMatriz;

    public Vista() {
        //arbol.inicializar();
        this.setTitle("Codigo de Huffman");
        addComponents();
        
        initComponents();
        this.setSize(1525, 720);
    }

    @Override
    public void paint(Graphics gp) {
        super.paint(gp);
        if (draw) {

            this.getContentPane().removeAll();

            add(palabra);
            add(insertar);
            add(dibujar);
            add(mostrarInfo);
            add(mostrarCodigo);
            add(mostrarMatriz);

            ArrayList<String> posordenData = new ArrayList<String>();
            ArrayList<String> nodo = huffman.dataOrden;
            System.out.println("TAMAÑO DE ARREGLO" + nodo.size());
            int size = huffman.altura();

            List<Integer> posIniciales = new ArrayList<Integer>(size);
            List<Integer> saltos = new ArrayList<Integer>(size);

            int val = 0;
            int anterior = 25;
            for (int j = 0; j < size; j++) {
                posIniciales.add(j, val);
                val = val + anterior;
                anterior = anterior * 2;
                saltos.add(j, anterior);
            }

            for (int i = 0; i < nodo.size(); i++) {

                String nodoPuntos = nodo.get(i);
                String[] parts = nodoPuntos.split("-");
                a = new JLabel(parts[0]);
                b = new JLabel(parts[4]);
                //System.out.println("pa "+parts[3]);

                int position = size - Integer.parseInt(parts[2]);

                if ("!".equals(parts[3])) {
                    a.setBounds(posIniciales.get(position), (size - position) * 95, 30, 25);
                    b.setBounds(posIniciales.get(position) + 10, (size - position) * 95, 30, 25);
                } else {
                    a.setBounds((saltos.get(position) * (Integer.parseInt(parts[3]) - 1)) + posIniciales.get(position), (size - position) * 95, 30, 30);
                    b.setBounds((saltos.get(position) * (Integer.parseInt(parts[3]) - 1)) + 10 + posIniciales.get(position), (size - position) * 95, 30, 30);
                }

                add(a);
                add(b);
                //add(lineas);
            }

            //super.revalidate();
            super.paint(gp);

            val = 0;
            anterior = 25;
            posIniciales = new ArrayList<Integer>(size);
            for (int j = 0; j < size; j++) {
                posIniciales.add(j, val);
                val = val + anterior;
                anterior = anterior * 2;
                //saltos.add(j, anterior);
            }

            for (int i = 0; i < nodo.size(); i++) {
                String nodoPuntos = nodo.get(i);
                String[] parts = nodoPuntos.split("-");

                int position = size - Integer.parseInt(parts[2]);
                System.out.println("size " + size);
                if (!"!".equals(parts[3])) {
                    Graphics2D graphics = (Graphics2D) gp;
                    Line2D lineaD = null;
                    int hD = (Integer.parseInt(parts[3]) * 2);
                    int posAnterior = 0;
                    int posInicialX = ((saltos.get(position) * (Integer.parseInt(parts[3]) - 1)) + posIniciales.get(position)) + 10;
                    int posInicialY = ((size - position) * 95) + 35;
                    int posFinalX = 0;
                    int posFinalY = 0;
                    if ("0".equals(parts[1])) {
                        posAnterior = (Integer.parseInt(parts[3]) + 1) / 2;
                        //posFinalX = (((int) posIniciales.get(position - 1)) + saltos.get(position - 1) * (hD - 1)) + 15;
                    } else {
                        posAnterior = (Integer.parseInt(parts[3])) / 2;

                    }
                    System.out.println("i " + i + "pos" + position);
                    posFinalX = (posIniciales.get(position + 1) + (saltos.get(position + 1) * (posAnterior - 1))) + 12;
                    posFinalY = ((size - (position + 1)) * 95) + 55;

                    lineaD = new Line2D.Float(posInicialX, posInicialY, posFinalX, posFinalY);

                    graphics.draw(lineaD);

                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 769, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 483, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vista().setVisible(true);
            }
        });
    }

    private void addComponents() {
        Font fuente = new Font("Monospaced", Font.PLAIN, 12);
        Font fuente2 = new Font("Monospaced", Font.PLAIN, 10);
        palabra = new JTextField();
        palabra.setBounds(10, 10, 176, 25);
        //traduccion = new JTextField();
        //traduccion.setBounds(10, 40, 75, 25);

        insertar = new JButton("Insertar");
        insertar.setBounds(185, 10, 85, 25);

        mostrarInfo = new JTextArea();
        mostrarInfo.setBounds(400, 10, 180, 100);

        mostrarMatriz = new JTextArea();
        mostrarMatriz.setBounds(600, 850, 950, 150);
        mostrarMatriz.setFont(fuente2);

        mostrarCodigo = new JTextArea();
        mostrarCodigo.setBounds(750, 710, 750, 120);
        mostrarCodigo.setFont(fuente);

        insertar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String palabranueva = palabra.getText();
                mostrarInfo.setText(null);
                mostrarMatriz.setText(null);
                mostrarCodigo.setText(null);

                //huffman.borrar();
                huffman.iniciar(palabranueva);
                huffman.imprimir();
                huffman.crearOrden("!", 1, 1, 1, "");
                huffman.imprimir();

                int bitsInicial = palabranueva.length() * 8;
                int bitsFinal = huffman.cantidadBits();
                double intComprimido = bitsInicial - bitsFinal;
                double codificado = (intComprimido * 100) / bitsInicial;
                double ahorro = 100 - codificado;
                StringBuilder str = new StringBuilder();
                str.append("Bits Iniciales: ");
                str.append(String.valueOf(bitsInicial));
                str.append("\n");
                str.append("Comprimido: ");
                str.append(String.valueOf(bitsFinal));
                str.append("\n");
                str.append("Ahorrado: ");
                str.append(String.valueOf(codificado) + "%");
                str.append("\n");
                str.append("Codificado: ");
                str.append(String.valueOf(ahorro + "%"));
                mostrarInfo.setText(str.toString());

                StringBuilder str2 = new StringBuilder();
                huffman.mostrarDatosSep();
                str2.append("Codificación final: ");
                str2.append("\n");
                str2.append(huffman.codigoLetra);
                str2.append("\n");
                str2.append(huffman.codigo);
                str2.append("\n");
                str2.append(huffman.datosSeparados);
                str2.append("\n");
                str2.append(huffman.datosSeparados2);
                mostrarCodigo.setText(str2.toString());

                huffman.mostrarMatriz();
                StringBuilder str3 = new StringBuilder();
                str3.append("           ");
                for (int i = 0; i < huffman.fila1.size(); i++) {
                    if (i > 9) {
                        str3.append(i + "  |");
                    } else if(i==1) {
                        str3.append(i + "   |");
                    }else if(i==2){
                        str3.append(i + "  |");
                    }else{
                        str3.append(i + "   |");
                    }
                }
                str3.append("\n");
                str3.append("Simbolo    ");
                str3.append(huffman.fila1);
                str3.append("\n");
                str3.append("Frecuencia ");
                str3.append(huffman.fila2);
                str3.append("\n");
                str3.append("Padre      ");
                str3.append(huffman.fila3);
                str3.append("\n");
                str3.append("Tipo       ");
                str3.append(huffman.fila4);
                str3.append("\n");
                str3.append("Izquierda  ");
                str3.append(huffman.fila5);
                str3.append("\n");
                str3.append("Derecha    ");
                str3.append(huffman.fila6);
                mostrarMatriz.setText(str3.toString());

                //System.out.println("alto "+huffman.altura());
            }

        });

        dibujar = new JButton("Dibujar");
        dibujar.setBounds(185, 40, 85, 25);
        dibujar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //arbol.inicializar();
                draw = true;

                System.out.println("data " + huffman.dataOrden);

                repaint();
            }
        });
        add(mostrarInfo);
        add(palabra);
        add(insertar);
        add(dibujar);
        add(mostrarCodigo);
        add(mostrarMatriz);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
