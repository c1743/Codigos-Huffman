/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author AndresFWilT / Edwin Hernandez
 */
public class Huffman {

    static String letras[][];
    ArrayList<String> dataOrden = new ArrayList<String>();
    static boolean exist;
    static Vector<String> subCaracteres;
    static Vector<Integer> subFrecuencia;
    static String palabra;
    ArrayList<String> bits = new ArrayList<String>();
    String codigo = "", letraEv, codigoLetra = "";

   
    
    
    public Huffman() {
        //subCaracteres = new Vector<String>();
    }

    public void iniciar(String palabra) {
        this.palabra = palabra;
        subCaracteres = new Vector<String>();
        for (int i = 0; i < palabra.length(); i++) {
            //System.out.println("letra " + String.valueOf(palabra.charAt(i)));
            frecuencia(String.valueOf(palabra.charAt(i)));
        }
        ordenar(subCaracteres);
        inicializar();
        llenar();
        completar();
        imprimir();
    }

    private static void frecuencia(String letra) {
        exist = false;
        for (int i = 0; i < subCaracteres.size(); i++) {
            if (subCaracteres.get(i).equals(letra)) {
                exist = true;
                break;
            }
        }
        if (!exist) {
            subCaracteres.add(letra);
            exist = false;
        }
        //System.out.println("tamaño " + subCaracteres.size());
    }

    private static void ordenar(Vector<String> subCaracteres) {
        for (int i = 0; i < subCaracteres.size(); i++) {
            for (int j = 0; j < subCaracteres.size() - 1; j++) {
                //boolean esMayor = false;

                char letraA = subCaracteres.get(j).charAt(0);
                char letraB = subCaracteres.get(j + 1).charAt(0);
                int asciiValue1 = (int) letraA;
                int asciiValue2 = (int) letraB;
                if (asciiValue1 > asciiValue2) {

                    subCaracteres.set(j, String.valueOf(letraB));
                    subCaracteres.set(j + 1, String.valueOf(letraA));

                }

                //System.out.println("letra2 "+letra1 );
            }
        }
        //System.out.println("Palabra: "+subCaracteres);
    }

    private static void inicializar() {
        int size = subCaracteres.size();
        size = (size * 2) - 1;
        letras = new String[7][size];
    }

    private static void llenar() {
        int fin;
        fin = ((subCaracteres.size() * 2) - 1);
        //llenar letras ordenadas
        for (int i = 0; i < subCaracteres.size(); i++) {
            letras[0][i] = subCaracteres.get(i);
        }
        //llenar frecuencia de letras
        for (int i = 0; i < subCaracteres.size(); i++) {
            //String let = letras[0][i];
            int count = palabra.length() - palabra.replaceAll((letras[0][i]), "").length();
            letras[1][i] = String.valueOf(count);
        }
        for (int i = 0; i < fin; i++) {
            //String let = letras[0][i];
            letras[6][i] = "NO";
        }
        System.out.println("arr " + letras[0].length);
        for (int i = 0; i < letras.length; i++) {
            for (int j = 0; j < letras[i].length; j++) {
                System.out.print(" " + letras[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static int saberMenor(int aumento) {
        int menor = 10000;
        int pos1 = 0;
        for (int i = 0; i < subCaracteres.size() + aumento; i++) {
            //System.out.println("POSICION PA FINAL"+subCaracteres.size()+aumento);
            if (parseInt(letras[1][i]) < menor && letras[6][i] == "NO") {
                menor = parseInt(letras[1][i]);
                //  System.out.println("MENOR "+menor+"EN POS: "+i);
                pos1 = i;
            }
        }
        letras[6][pos1] = "YA";
        return pos1;
    }

    private static void sumatoria(int aumento) {
        int pos = saberMenor(aumento);
        int pos2 = saberMenor(aumento);
        int num1 = 0, num2 = 0;
        for (int i = 0; i < subCaracteres.size() + aumento; i++) {
            if (pos == i) {
                num1 = parseInt(letras[1][i]);
            }
            if (pos2 == i) {
                num2 = parseInt(letras[1][i]);
            }
        }
        letras[1][subCaracteres.size() + aumento] = String.valueOf(num1 + num2);
        letras[4][subCaracteres.size() + aumento] = String.valueOf(pos);
        letras[5][subCaracteres.size() + aumento] = String.valueOf(pos2);

        letras[2][pos] = String.valueOf(subCaracteres.size() + aumento);
        letras[3][pos] = String.valueOf(1);

        letras[2][pos2] = String.valueOf(subCaracteres.size() + aumento);
        letras[3][pos2] = String.valueOf(2);
    }

    public static void imprimir() {
        for (int x = 0; x < letras.length; x++) {
            System.out.print("");
            for (int y = 0; y < letras[x].length; y++) {
                System.out.print(letras[x][y]);
                if (y != letras[x].length - 1) {
                    System.out.print("\t");
                }
            }
            System.out.println("");
        }
    }

    public int altura() {
        int alturaArbol = 0;
        for (int i = 0; i < dataOrden.size(); i++) {
            String nodoPuntos = dataOrden.get(i);
            String[] parts = nodoPuntos.split("-");
            int altura = Integer.parseInt(parts[2]);
            if (altura > alturaArbol) {
                alturaArbol = altura;
            }
        }
        return alturaArbol;
    }

    private static void completar() {
        int fin;
        fin = ((subCaracteres.size() * 2) - 1) - subCaracteres.size();
        for (int i = 0; i < fin; i++) {
            sumatoria(i);
        }
    }

     void datos() {
        int bitsInicial = palabra.length() * 8;

    }

    void crearOrden(String posLetra, int pos, int nivel, int col, String codigo) {
        if (posLetra.equals("!")) {
            dataOrden.add(letras[1][letras[0].length - 1] + "-" + "1" + "-" + "1" + "-" + "!"+ "-" +" ");
            if (letras[4][letras[0].length - 1] != null) {
                crearOrden(letras[4][letras[0].length - 1], 0, nivel + 1, 1, "0");
            }
            if (letras[5][letras[0].length - 1] != null) {
                crearOrden(letras[5][letras[0].length - 1], 1, nivel + 1, 2, "1");
            }
        } else {
            int dir = Integer.parseInt(posLetra);
            if (letras[4][dir] == null || letras[5][dir] == null) {
                dataOrden.add(letras[0][dir] + "-" + pos + "-" + nivel + "-" + col + "-" + " ");
                letras[6][dir] = codigo;
            } else {
                dataOrden.add(letras[1][dir] + "-" + pos + "-" + nivel + "-" + col+ "-" + " ");
                if (letras[4][dir] != null) {
                    crearOrden(letras[4][dir], 0, nivel + 1, (col * 2) - 1, codigo + 0);
                }
                if (letras[5][dir] != null) {
                    crearOrden(letras[5][dir], 1, nivel + 1, (col * 2), codigo + 1);
                }
            }
        }
    }

    
    
    ArrayList<String> datosSeparados = new ArrayList<String>();
    ArrayList<String> datosSeparados2 = new ArrayList<String>();
    void mostrarDatosSep() {
        for (int i = 0; i < subCaracteres.size(); i++) {
            if (i > subCaracteres.size() / 2) {
                datosSeparados2.add(letras[0][i] + " = " + letras[6][i]);
            } else {
                datosSeparados.add(letras[0][i] + " = " + letras[6][i]);
            }
        }
    }
       
  
    
    int cantidadBits() {
        for (int i = 0; i < palabra.length(); i++) {
            letraEv = Character.toString(palabra.charAt(i));
            for (int j = 0; j < subCaracteres.size(); j++) {
                if (letraEv.equals(letras[0][j])) {
                    codigo = codigo + letras[6][j];
                    codigoLetra = codigoLetra + letras[0][j];
                    for (int k = 1; k < letras[6][j].length(); k++) {
                        codigoLetra = codigoLetra + " ";
                    }
                }
            }
        }
        System.out.println(codigoLetra);
        System.out.println(codigo);
        return codigo.length();
    }
//        void borrar() {
//        dataOrden = null;
//        subCaracteres = null;
//        subFrecuencia = null;
//        palabra = null;
//        bits = null;
//        codigo = null;
//        letraEv = null;
//        codigoLetra = null;
//        letras=null;
//    }
    
    ArrayList<String> fila1 = new ArrayList<String>();
    ArrayList<String> fila2 = new ArrayList<String>();
    ArrayList<String> fila3 = new ArrayList<String>();
    ArrayList<String> fila4 = new ArrayList<String>();
    ArrayList<String> fila5 = new ArrayList<String>();
    ArrayList<String> fila6 = new ArrayList<String>();
    
    void mostrarMatriz() {
        for (int x = 0; x < letras[0].length; x++) {
            if (letras[0][x] == null) {
                if (x > 9) {
                    fila1.add(x + "|");
                } else {
                    fila1.add(x + " |");
                }
            } else {
                if (letras[0][x].length() > 1) {
                    fila1.add(letras[0][x] + "|");
                } else {
                    fila1.add(letras[0][x] + " |");
                }
            }
            if (letras[1][x] == null) {
                fila2.add(0 + " |");
            } else {
                if (letras[1][x].length() > 1) {
                    fila2.add(letras[1][x] + "|");
                } else {
                    fila2.add(letras[1][x] + " |");
                }
            }
            if (letras[2][x] == null) {
                fila3.add(0 + " |");
            } else {
                if (letras[2][x].length() > 1) {
                    fila3.add(letras[2][x] + "|");
                } else {
                    fila3.add(letras[2][x] + " |");
                }
            }
            if (letras[3][x] == null) {
                fila4.add(0 + " |");
            } else {
                fila4.add(letras[3][x] + " |");
            }
            if (letras[4][x] == null) {
                fila5.add(0 + " |");
            } else {
                if (letras[4][x].length() > 1) {
                    fila5.add(letras[4][x] + "|");
                } else {
                    fila5.add(letras[4][x] + " |");
                }
            }
            if (letras[5][x] == null) {
                fila6.add(0 + " |");
            } else {
                if (letras[5][x].length() > 1) {
                    fila6.add(letras[5][x] + "|");
                } else {
                    fila6.add(letras[5][x] + " |");
                }
            }
        }
    }

}
